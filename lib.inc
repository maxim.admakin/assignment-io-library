exit: 
    mov rax, 60
    syscall
    ret 

string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .exit
        inc rax
        jmp short .loop
    .exit:
        ret

print_string:
    push rsi
    push rdi
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1 
    mov rdi, 1
    syscall
    pop rdi
    pop rsi
    ret

print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1 
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

print_uint:
    push rbx 
    push rbp
    mov rbp, rsp
    mov rax, rdi
    mov rbx, 10
    dec rsp
    mov byte [rsp], 0
    .loop:
        xor rdx, rdx
        div rbx
        add rdx, 48
        dec rsp
        mov [rsp], dl
        test rax, rax
        jz .write_number
        jmp short .loop
    .write_number:
        mov rdi, rsp
        call print_string
        mov rsp, rbp
        pop rbp
        pop rbx
        ret

print_int:
    cmp rdi, 0
    jl .lessZero
    call print_uint
    ret
    .lessZero:
        push rdi
        mov rdi,'-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret

string_equals:
        xor     rax, rax
        push    r8
        push    r9
        push rdi
        push rsi
        .loop:
            mov     r8b, byte[rsi]
            mov     r9b, byte[rdi]
            inc     rsi
            inc     rdi
            cmp     r8b, r9b
            jne     .exit
            cmp     r9b, 0
            jne     .loop
            inc     rax
        .exit:
            pop rsi
            pop rdi
            pop r9
            pop r8
            ret

read_char:
    push rdi
    push rsi
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    test rax, rax
    je .exit
    xor rax, rax
    mov al, [rsp]
    .exit:
        inc rsp
        pop rsi
        pop rdi
        ret

read_word:
    push r8
    push r9
    push r10
    xor     r9, r9
    mov     r10, rsi
    mov     r8, rdi
    .loop:
        call    read_char
        cmp     al, 0x20
        je      .loop
        cmp     al, 0x9
        je      .loop
        cmp     al, 0xA
        je      .loop
        cmp     al, 0x0
        je      .end_read
        jmp     .write_c
    .start_read:
        call    read_char
        cmp     al, 0x0
        je      .end_read
        cmp     al, 0x20
        je      .end_read
        jmp     .write_c
    .write_c:
        mov     byte [r8 + r9], al
        inc     r9
        cmp     r9, r10
        je      .of
        jmp     .start_read
    .end_read:
        mov     rax, r8
        mov     byte [r8 + r9], 0
        mov     rdx, r9
        jmp     .exit
    .of:
        xor     rax, rax
        xor     rdx, rdx
    .exit:
    pop r10
    pop r9
    pop r8
        ret

parse_uint:
    xor     rax, rax
    push    r9
    mov     r9, 10
    xor     rax, rax
    xor     rcx, rcx
    xor     rdx, rdx
    xor     rsi, rsi
    .parse_char:
        mov     sil, [rdi + rcx]
        cmp     sil, 0x30
        jl      .return
        cmp     sil, 0x39
        jg      .return
        inc     rcx
        sub     sil, 0x30
        mul     r9
        add     rax, rsi
        jmp     .parse_char
    .return:
        mov     rdx, rcx
        pop    r9
        ret

parse_int:
    xor rax, rax
    cmp byte[rdi], 0x2d
    je .neg_parse
    call parse_uint
    ret
    .neg_parse:
        inc rdi
        call parse_uint
        cmp     rdx, 0
        je      .return
        neg     rax
        inc     rdx
    .return:
        ret

string_copy:
    push r8
    xor rcx, rcx
    .loop:
        cmp rcx, rdx  
        je .of  
        mov r8, [rdi + rcx] 
        mov [rsi, rcx], r8 
        cmp r8, 0
        je .ok  
        inc rcx
        jmp .loop
    .of:
        xor rax, rax
        jmp .exit
    .ok:
        mov rax, rcx
    .exit:
        pop r8
        ret
